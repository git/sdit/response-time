// [!output PROJECT_NAME].cpp : main project file.

#include "stdafx.h"
#include "Form1.h"

using namespace [!output SAFE_NAMESPACE_NAME];

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	Application::Run(gcnew [!output CLASS_NAME_PREFIX]Form1());
	return 0;
}
