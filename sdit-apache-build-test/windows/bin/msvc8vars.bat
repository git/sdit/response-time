rem ----------------------------------------------------------------
rem Microsoft Express Edition  
rem ----------------------------------------------------------------
echo "----------------------------------------------------------------"
echo " Microsoft Express Edition MSVC8 aka Visual Studio 9.0"
echo "----------------------------------------------------------------"
SET VSINSTALLDIR=%~dp0Program Files\Microsoft Visual Studio 9.0
@REM Shorten path to 8.3 names to remove spaces.
@for %%A in ("%VSINSTALLDIR%") do set MY_TMP=%%~sA
SET VSINSTALLDIR=%MY_TMP%

SET WindowsSdkDir=%~dp0Program Files\Microsoft SDKs\Windows\v6.0A
@REM Shorten path to 8.3 names to remove spaces.
@for %%A in ("%WindowsSdkDir%") do set MY_TMP=%%~sA
SET WindowsSdkDir=%MY_TMP%

SET DevEnvDir=%VSINSTALLDIR%\Common7\IDE
SET Framework35Version=v3.5
SET FrameworkDir=%SYSTEMROOT%\Microsoft.NET\Framework
SET FrameworkVersion=v2.0.50727
SET INCLUDE=%VSINSTALLDIR%\VC\INCLUDE;%WindowsSdkDir%\include
SET LIB=%VSINSTALLDIR%\VC\LIB;%WindowsSdkDir%\lib
SET LIBPATH=C:\WINDOWS\Microsoft.NET\Framework\v3.5
SET PATH=%VSINSTALLDIR%\Common7\IDE;%VSINSTALLDIR%\VC\BIN;%VSINSTALLDIR%\Common7\Tools;%SYSTEMROOT%\Microsoft.NET\Framework\v3.5;%VSINSTALLDIR%\VC\VCPackages;%WindowsSdkDir%\bin;%~dp0Python25\;C:\LSF_6.2\bin;%~dp0perl\bin;%SYSTEMROOT%\system32;%SYSTEMROOT%;%SYSTEMROOT%\System32\Wbem;C:\SFU\bin\;C:\SFU\Perl\bin\;C:\SFU\common\
SET VS90COMNTOOLS=%VSINSTALLDIR%\Common7\Tools\