#!/bin/bash
HOST=$1
CPP=msvc2010
MNT=nfs
DRV=Z
TOTAL=1
LOCAL=no

if [ "$HOST" == "" ]; then
  echo "$0 <host_name> [total_to_submit] "
  exit
fi
HOST=$HOST.dal.design.ti.com

if [ "$2" != "" ]; then
  TOTAL=$2 
fi

if [ "$LOCAL" == "no" ]; then
  TAG=lsf.$MNT.$CPP.$TOTAL.remote.$$
else
  TAG=lsf.$MNT.$CPP.$TOTAL.local.$$
fi  


c=0
while [ $c -lt $TOTAL ]
do
  if [ "$LOCAL" == "no" ]; then
    bsub -q windows -m $HOST "\\\\samba_svr.dal.design.ti.com\\sdit\\a0323088\\sdit-apache-build-test\\build.bat -$MNT $DRV: -tag $TAG -$CPP"
  else
    bsub -q windows -m $HOST "\\\\samba_svr.dal.design.ti.com\\sdit\\a0323088\\sdit-apache-build-test\\build.bat -local -$MNT $DRV: -tag $TAG -$CPP"
  fi  
  c=$[c+1]
done
