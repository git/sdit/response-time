#!/bin/bash
HOST=$1
TOTAL=$2
CMD=$3

if [ "$HOST" == "" -o "$CMD" == "" -o "$TOTAL" == "" ]; then
  echo "$0 <host_name> <total_to_submit> <command>"
  exit
fi
HOST=$HOST.dal.design.ti.com
TOTAL=$2
CMD=$3

c=0
while [ $c -lt $TOTAL ]
do
  bsub -q windows -m $HOST $CMD
  c=$[c+1]
done
