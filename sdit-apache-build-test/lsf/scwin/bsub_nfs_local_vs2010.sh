#!/bin/bash
CPP=msvc2010
MNT=nfs
DRV=Z
TOTAL=1
LOCAL=yes

# number of jobs to submit
if [ "$1" != "" ]; then
  TOTAL=$1 
fi

if [ "$LOCAL" == "no" ]; then
  TAG=lsf.$MNT.$CPP.$TOTAL.remote.$$
else
  TAG=lsf.$MNT.$CPP.$TOTAL.local.$$
fi  


c=0
while [ $c -lt $TOTAL ]
do
  if [ "$LOCAL" == "no" ]; then
    bsub -q s_scwin -R win7 "\\\\samba_svr.dal.design.ti.com\\sdit\\a0323088\\sdit-apache-build-test\\build.bat -$MNT $DRV: -tag $TAG -$CPP"
  else
    bsub -q s_scwin -R win7 "\\\\samba_svr.dal.design.ti.com\\sdit\\a0323088\\sdit-apache-build-test\\build.bat -local -$MNT $DRV: -tag $TAG -$CPP"
  fi  
  c=$[c+1]
done
