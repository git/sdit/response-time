#!/usr/bin/perl
use POSIX qw(strftime);
use strict;
use Sys::Hostname;
use Cwd;
use Time::HiRes qw(usleep gettimeofday tv_interval);
use Getopt::Long;
use File::Path;
use File::Spec;
use Config;
use Fcntl ':flock';
$|=1;

# defaults vars
my (%vars);
%vars = (
"apache" => "httpd-2.2.14",
"host" => hostname,
"begindate" => strftime("%m/%d/%Y", localtime),
"begintime" => strftime("%H:%M:%S", localtime),
"begintd" => [gettimeofday],
"ddate" => strftime("%m_%d_%Y", localtime),
"dtime" => strftime("%H%M%S", localtime),
"builddir" => "./tmp",
"resultsdir" => "./tmp",
"tag" => "notag",
"MAKEFLAGS" => $ENV{'MAKEFLAGS'}
);

# call main an exit
exit(main());

###########################################################################################
# returns the full path to the directory
sub get_abs_path
{
  my $path = $_[0];
  return File::Spec->rel2abs($path);
}

###########################################################################################
# creates directories if needed and returns the full path
sub create_directory
{
  my $dir = $_[0];
  if (! -e $dir) 
  {  
    mkpath($dir) || die "Could not create directory: $dir\n"; 
  }
  return get_abs_path($dir);
}

###########################################################################################
sub timeit
{
  # vars
  my $counter = $_[0];
  my $cmdline = $_[1];
  my $t0 = [gettimeofday];
  print "Running: $cmdline\n";
  $vars{"$counter" . "_begin"} = strftime("%H:%M:%S", localtime);
  my $retVal = system($cmdline . " 2>&1");
  my $t1 = [gettimeofday];
  my $sec = tv_interval($t0, $t1);
  $vars{"$counter" . "_end"} = strftime("%H:%M:%S", localtime);
  $vars{"$counter" . "_total_seconds"} = $sec;
  $vars{"$counter" . "_elapsed"} = ($sec/(60*60))%24 . ":" . ($sec/60)%60 . ":" . $sec%60;
  $vars{"$counter" . "_elapsed_seconds"} = $sec;
  print "Elapsed time for last command: " . $vars{"$counter" . "_elapsed"} . "\n";
  return $retVal;
}

###########################################################################################
sub initialize
{
  # get options from command line
  GetOptions(\%vars, "tag=s", "local=s", "delete=s");

  # vars
  $vars{"osname"} = "windows" if (lc($Config{osname}) eq "mswin32");
  $vars{"osname"} = "linux" if (lc($Config{osname}) eq "linux");
  $vars{"buildname"} = "build." . $vars{"dtime"} . ".$$";
  $vars{"log_file"} = $vars{"buildname"} . ".log";

  # should we delete build files when done
  $vars{"delete"} = "yes" if (!defined($vars{"delete"}) || lc($vars{"delete"}) ne "no");
  $vars{"delete"} = "no" if (defined($vars{"delete"}) && lc($vars{"delete"}) eq "no");

  # should we copy local or not
  $vars{"local"} = "yes" if (defined($vars{"local"}) && lc($vars{"local"}) eq "yes");
  $vars{"local"} = "no" if (!defined($vars{"local"}) || lc($vars{"local"}) ne "yes");
  $vars{"builddir"} = "c:\\temp\\ApacheBuildTest" if ($vars{"local"} eq "yes" && $vars{"osname"} eq "windows");
  $vars{"builddir"} = "/tmp/ApacheBuildTest" if ($vars{"local"} eq "yes" && $vars{"osname"} eq "linux");

  # source
  $vars{"sourcedir"} = get_abs_path("./" . $vars{"osname"} . "/src/apache/" . $vars{"apache"});

  # build
  $vars{"build_hostdir"} = create_directory($vars{"builddir"} . "/" . $vars{"host"});
  $vars{"build_datedir"} = create_directory($vars{"build_hostdir"} . "/" . $vars{"ddate"});
  $vars{"build_tagdir"} = create_directory($vars{"build_datedir"} . "/" . $vars{"tag"});
  $vars{"build_builddir"} = create_directory($vars{"build_tagdir"} . "/" . $vars{"buildname"});
  $vars{"build_logfile"} = $vars{"build_tagdir"} . "/" . $vars{"log_file"};

  # results
  $vars{"results_hostdir"} = create_directory($vars{"resultsdir"} . "/" . $vars{"host"});
  $vars{"results_datedir"} = create_directory($vars{"results_hostdir"} . "/" . $vars{"ddate"});
  $vars{"results_tagdir"} = create_directory($vars{"results_datedir"} . "/" . $vars{"tag"});
  $vars{"results_builddir"} = create_directory($vars{"results_tagdir"} . "/" . $vars{"buildname"});
  $vars{"results_csv_file"} = get_abs_path($vars{"resultsdir"} . "/build_results.csv");

  # change / to \ for windows
  if ($vars{"osname"} eq "windows") 
  { 
    foreach my $key (keys(%vars))
    {
      $vars{$key} =~ s/\//\\/g; 
    }
  }

  # start logging
  print "Starting: $vars{begindate} $vars{begintime}\n";
  open(OLDOUT, ">&STDOUT") || die "Error saving STDOUT\n";
  open(OLDERR, ">&STDERR") || die "Error saving STDERR\n"; 
  open(STDOUT, ">$vars{build_logfile}") || die "Error opening logfile: " . $vars{"build_logfile"} . "\n";
  open(STDERR, ">&STDOUT") || die "Error directing STDERR to STDOUT\n";

  # output all vars
  foreach my $key (keys(%vars))
  {
    print "OPTION: $key = $vars{$key}\n";
  }
}

###########################################################################################
sub cleanup
{
  # stop logging
  close(STDOUT) || die "Error closing STDOUT\n";
  close(STDERR) || die "Errir closing STDERR\n";
  open(STDERR, ">&OLDERR") || die "Error restoring STDERR\n";
  open(STDOUT, ">&OLDOUT") || die "Error restoring STDOUT\n";
  close(OLDOUT) || die "Error closing OLDOUT\n";
  close(OLDERR) || die "Error closing OLDERR\n";

  # change to start dir so we can delete build and results dirs
  
  chdir($vars{"start_cwd"});
  if ($vars{"osname"} eq "windows")
  {
    chdir("c:\\");
  }
  else
  {
    chdir("/");
  }

  # copy logfile if build was local
  if ($vars{"local"} eq "yes" && $vars{"osname"} eq "windows")
  {
    timeit("copy_log", "xcopy /q \"" . $vars{"build_logfile"} . "\" \"" . $vars{"results_tagdir"} . "\\\"");
  }
  elsif ($vars{"local"} eq "yes")
  {
    timeit("copy_log", "cp \"" . $vars{"build_logfile"} . "/.\" \"" . $vars{"results_tagdir"} . "/\"");
  }

  # remove the build directory
  if (-e $vars{"build_builddir"} && $vars{"delete"} eq "yes")
  {
    print "Removing: $vars{build_builddir}\n";
    if ($vars{"osname"} eq "windows")
    {
      timeit("delete_build", "cmd.exe /c rmdir /q /s \"" . $vars{"build_builddir"} . "\"");
    }
    else
    {
      timeit("delete_build", "rm -rf \"" . $vars{"build_builddir"} . "\"");
    }
  }

  # remove results directory
  if (-e $vars{"results_builddir"} && $vars{"local"} eq "yes" && $vars{"delete"} eq "yes")
  {
    print "Removing: $vars{results_builddir}\n";
    if ($vars{"osname"} eq "windows")
    {
      timeit("delete_results", "cmd.exe /c rmdir /q /s \"" . $vars{"results_builddir"} . "\"");
    }
    else
    { 
      timeit("delete_results", "rm -rf \"" . $vars{"results_builddir"} . "\"");
    }
  }

  # remove logfile if build was local
  if ($vars{"local"} eq "yes" && $vars{"osname"} eq "windows" && $vars{"delete"} eq "yes")
  {
    timeit("delete_log", "del \"" . $vars{"build_logfile"} . "\"");
  }
  elsif ($vars{"local"} eq "yes")
  {
    timeit("delete_log", "rm -f \"" . $vars{"build_logfile"} . "\"");
  }

  # print results to screen
  print $vars{"headerline"} . "\n";
  print $vars{"csvline"} . "\n";

  # remove temporary build directories if they are empty
  rmdir($vars{"build_builddir"});
  rmdir($vars{"build_tagdir"});
  rmdir($vars{"build_datedir"});
  rmdir($vars{"build_hostdir"});
  rmdir($vars{"results_builddir"});
  rmdir($vars{"results_tagdir"});
  rmdir($vars{"results_datedir"});
  rmdir($vars{"results_hostdir"});
}

###########################################################################################
sub print_results
{
  my @csv = @_;

  # Human readable
  print "\nResults\n";
  foreach my $key (keys(%vars))
  {
    print "$key: $vars{$key}\n";
  }

  # CSV format
  print "\n\n***** Results *****\n";
  my $headerline = "";
  my $csvline = "";
  foreach my $key (@csv)
  {
    if ($csvline eq "")
    {
      $headerline = $key;
      $csvline = $vars{$key};
    }
    else
    {
      $headerline .= ", " . $key;
      $csvline .= ", " . $vars{$key};
    }
  }
  print $headerline . "\n";
  print $csvline . "\n";

  # set vars
  $vars{"headerline"} = $headerline;
  $vars{"csvline"} = $csvline;
}

###########################################################################################
sub build_apache
{
  # vars
  my $retVal = 0;

  # copy source to build dir
  if ($vars{"osname"} eq "windows")
  {
    timeit("copy_source", "xcopy /q /s \"" . $vars{"sourcedir"} . "\" \"" . $vars{"build_builddir"} . "\"");
  }
  else
  {
    timeit("copy_source", "cp -r \"" . $vars{"sourcedir"} . "/.\" \"" . $vars{"build_builddir"} . "\"");
  }
  
  # cd to build dir
  chdir($vars{"build_builddir"}) || die "Could not change directory to: " . $vars{"build_builddir"} . "\n";

  # build 
  if ($vars{"osname"} eq "windows")
  {
    timeit("build", "nmake /f Makefile.win _apacher");
    if (! -e "Release/httpd.exe")
    {
      print "Error: Build failed: Release/httpd.exe does not exist\n";
      $vars{build_success} = "no";
    }
    else
    {
      print "Build was successfull: Release/httpd.exe exists\n";
      $vars{build_success} = "yes";
    }  
  }
  else
  {
    timeit("config", "./configure");
    timeit("build", "make $vars{MAKEFLAGS}");
    if (! -e "httpd")
    {
      print "Error: Build failed: httpd does not exist\n";
      $vars{build_success} = "no";
    }
    else
    {
      print "Build was successfull: httpd exists\n";
      $vars{build_success} = "yes";
    }  
  }

  # see if we have a good build

  # copy back if it was a local build
  if ($vars{"local"} eq "yes" && $vars{"osname"} eq "windows")
  {
    timeit("copy_results", "xcopy /q /s \"" . $vars{"build_builddir"} . "\" \"" . $vars{"results_builddir"} . "\"");
  }
  elsif ($vars{"local"} eq "yes")
  {
    timeit("copy_results", "cp -r \"" . $vars{"build_builddir"} . "/.\" \"" . $vars{"results_builddir"} . "\"");
  }

  # return
  return $retVal;
}

###########################################################################################
###########################################################################################
###########################################################################################
sub main
{
  # initialize
  initialize();

  # build apache
  my $retVal = build_apache();

  # set end time
  $vars{"endtime"} = strftime("%H:%M:%S", localtime);
  my $enddt = [gettimeofday];
  my $sec = tv_interval($vars{begintd}, $enddt);
  $vars{"total_time"} = ($sec/(60*60))%24 . ":" . ($sec/60)%60 . ":" . $sec%60;
  $vars{"total_time_seconds"} = $sec;

  # print results
  print_results(("host", "begindate", "begintime", "endtime", "total_time", "total_time_seconds", "tag", "osname", "copy_source_begin", "copy_source_end", "copy_source_elapsed", "copy_source_elapsed_seconds", "build_begin", "build_end", "build_elapsed", "build_elapsed_seconds", "build_success", "copy_results_begin", "copy_results_end", "copy_results_elapsed", "copy_results_elapsed_seconds"));

  # cleanup
  cleanup();

  # done
  return $retVal;
}  
