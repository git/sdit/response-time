@echo off

rem ****** default vars
set drive=
set tag=notag
set local=no
set cifs=\\dflfs04\sdotools
set nfs=\\near11.dal.design.ti.com\vol\fvol84\sdit\a0323088
set scriptdir=\sdit-apache-build-test
set perl=%~dp0Windows\bin\Perl\bin\perl.exe
set vcvars=%~dp0Windows\bin\msvc8vars.bat
set delete=yes

rem ************************************************************************************
:params
if "%1"=="" goto :build
if "%1"=="-cifs" goto :cifs
if "%1"=="-nfs" goto :nfs
if "%1"=="-msvc6" goto :msvc6
if "%1"=="-msvc2003" goto :msvc2003
if "%1"=="-msvc2008" goto :msvc2008
if "%1"=="-msvc2010" goto :msvc2010
if "%1"=="-tag" goto :tag
if "%1"=="-local" goto :local
if "%1"=="-donotdelete" goto :donotdelete
if "%1"=="--help" goto :help
if "%1"=="-help" goto :help
if "%1"=="-h" goto :help
if "%1"=="/?" goto :help
shift
goto :params

rem ************************************************************************************
:donotdelete
shift
set delete=no
goto :params

rem ************************************************************************************
:local
shift
set local=yes
goto :params

rem ************************************************************************************
:tag
shift
set tag=%1
goto :params

rem ************************************************************************************
:msvc6
shift
if exist "C:\Program Files (x86)\Microsoft Visual Studio\VC98\Bin\vcvars32.bat" set vcvars="C:\Program Files (x86)\Microsoft Visual Studio\VC98\Bin\vcvars32.bat"
if exist "C:\Program Files\Microsoft Visual Studio\VC98\Bin\vcvars32.bat" set vcvars="C:\Program Files\Microsoft Visual Studio\VC98\Bin\vcvars32.bat"
goto :params

rem ************************************************************************************
:msvc2003
shift
if exist "C:\Program Files (x86)\Microsoft Visual Studio .NET 2003\Common7\Tools\vsvars32.bat" set vcvars="C:\Program Files (x86)\Microsoft Visual Studio .NET 2003\Common7\Tools\vsvars32.bat"
if exist "C:\Program Files\Microsoft Visual Studio .NET 2003\Common7\Tools\vsvars32.bat" set vcvars="C:\Program Files\Microsoft Visual Studio .NET 2003\Common7\Tools\vsvars32.bat"
goto :params

rem ************************************************************************************
:msvc2008
shift
if exist "C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat" set vcvars="C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat"
if exist "C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat" set vcvars="C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat"
goto :params

rem ************************************************************************************
:msvc2010
shift
if exist "C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\Tools\vsvars32.bat" set vcvars="C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\Tools\vsvars32.bat"
if exist "C:\Program Files\Microsoft Visual Studio 10.0\Common7\Tools\vsvars32.bat" set vcvars="C:\Program Files\Microsoft Visual Studio 10.0\Common7\Tools\vsvars32.bat"
goto :params

rem ************************************************************************************
:cifs
shift
set drive=%1
if "%drive%"=="" goto :help
net use %drive% /delete /yes
net use %drive% %cifs%
%drive%
cd %scriptdir%
goto :params

rem ************************************************************************************
:nfs
shift
set drive=%1
if "%drive%"=="" goto :help
net use %drive% /delete /yes
net use %drive% %nfs%
%drive%
cd %scriptdir%
goto :params

rem ************************************************************************************
:build
echo VCVARS: %vcvars%
echo Perl: %perl%
if exist %vcvars% call %vcvars%
echo Running: %perl% build.pl -tag=%tag% -local=%local% -delete=%delete%
%perl% build.pl -tag=%tag% -local=%local% -delete=%delete%
goto :end

rem ************************************************************************************
:help
echo.
echo "build_apache.bat [-tag tagname] [-cifs|-nfs drive_letter] [-msvc2010|-msvc2008|-msvc2003|-msvc6] [-local] [-donotdelete]"
echo.
echo  -tag         : default is notag
echo  -cifs,-nfs   : uses a shared location for the build. For example, -cifs z:
echo  -msvc2010,
echo  -msvc2008,
echo  -msvc2003,
echo  -msvc6       : tries to use a locally installed compiler from c:\program files
echo               :  the default is to use msvc8 from .\windows\bin 
echo  -local       : copies the files to c:\temp\ApacheBuildTest before compiling
echo  -donotdelete : leaves the source directory and built files after building;
echo               :  the default is to delete the files
goto :finish

rem ************************************************************************************
:end
if "%drive%"=="" goto :finish
c:
net use %drive% /delete /yes
:finish



