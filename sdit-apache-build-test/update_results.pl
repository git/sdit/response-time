#!/usr/bin/perl
use strict;
use FindBin qw($Bin); 
use File::Path;
use File::Glob ':glob';

my $script_path = $Bin;
my $results_path = "$script_path/results";
my $build_dirs = "$script_path/tmp";
my $results_file = "$results_path/all_results.csv";
my @results;

# call main an exit
exit(main());

###########################################################################################
sub process_log_file
{
  my $hostname = $_[0];
  my $log = $_[1];
  open(FILE, "<$log") || die "Could not open: $log\n";
  my @lines = <FILE>;
  close(FILE);
  chomp(@lines);
  if ($lines[scalar(@lines)-1] =~ m/^$hostname, /)
  {
    push(@results, $lines[scalar(@lines)-1]); 
  }
  else
  {
    print "Log file does not contain results: $log\n";
  }
}

###########################################################################################
###########################################################################################
###########################################################################################
sub main
{
  # make paths
  mkpath($results_path);
  
  # go though builds
  my @hosts = glob("$build_dirs/*");
  foreach my $host (@hosts)
  {
    my @dirs = split("/", $host);
    my $hostname = $dirs[scalar(@dirs)-1];
    print "Host: $hostname\n";
    my @dates = glob("$host/*");
    foreach my $date (@dates)
    {
      my @tags = glob("$date/*");
      foreach my $tag (@tags)
      {
        my @logs = glob("$tag/build.*.log");
        foreach my $log (@logs)
        {
          process_log_file($hostname, $log);
        }
      }
    }
  }

  # open results file
  if (scalar(@results) > 0)
  {
    open(RESULTS, ">$results_file") || die "Could not open file: $results_file\n";
    foreach my $line (@results)
    {
      print RESULTS "$line\n";
    }
    close(RESULTS);
  }

  # done
  return 0;
}  


